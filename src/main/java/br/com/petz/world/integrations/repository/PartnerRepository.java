package br.com.petz.world.integrations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.petz.world.integrations.model.Partner;

public interface PartnerRepository extends JpaRepository<Partner, Integer> {

}
