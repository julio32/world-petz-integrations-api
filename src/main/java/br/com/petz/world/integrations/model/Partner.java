package br.com.petz.world.integrations.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "partner", indexes = {
		@Index(name = "partner_name_partner_IX", columnList = "name_partner", unique = true),
		@Index(name = "partner_cnpj_IX", columnList = "cnpj", unique = true),
		@Index(name = "partner_email_partner_IX", columnList = "email_partner", unique = true) })
public class Partner implements Serializable {

	/** Primary key. */
	protected static final String PK = "partnerId";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "partner_id", unique = true, nullable = false, length = 10)
	private int partnerId;
	@Column(name = "name_partner", unique = true, nullable = false, length = 50)
	private String namePartner;
	@Column(unique = true, nullable = false, length = 18)
	private String cnpj;
	@Column(nullable = false, length = 3)
	private short active;
	@Column(name = "email_partner", unique = true, nullable = false, length = 50)
	private String emailPartner;
	@Column(name = "cell_phone", nullable = false, length = 14)
	private String cellPhone;
	@Column(nullable = false, length = 13)
	private String phone;
	@Column(name = "date_created", nullable = false)
	private Timestamp dateCreated;
	@Column(name = "date_updated")
	private Timestamp dateUpdated;
	@OneToMany(mappedBy = "partner")
	private Set<Product> product;
	@ManyToOne(optional = false)
	@JoinColumn(name = "id_pet", nullable = false)
	private Pet pet;
	@OneToMany(mappedBy = "partner")
	private Set<Stock> stock;
	@ManyToOne(optional = false)
	@JoinColumn(name = "store_id", nullable = false)
	private Store store;

	/** Default constructor. */
	public Partner() {
		super();
	}

	/**
	 * Access method for partnerId.
	 *
	 * @return the current value of partnerId
	 */
	public int getPartnerId() {
		return partnerId;
	}

	/**
	 * Setter method for partnerId.
	 *
	 * @param aPartnerId the new value for partnerId
	 */
	public void setPartnerId(int aPartnerId) {
		partnerId = aPartnerId;
	}

	/**
	 * Access method for namePartner.
	 *
	 * @return the current value of namePartner
	 */
	public String getNamePartner() {
		return namePartner;
	}

	/**
	 * Setter method for namePartner.
	 *
	 * @param aNamePartner the new value for namePartner
	 */
	public void setNamePartner(String aNamePartner) {
		namePartner = aNamePartner;
	}

	/**
	 * Access method for cnpj.
	 *
	 * @return the current value of cnpj
	 */
	public String getCnpj() {
		return cnpj;
	}

	/**
	 * Setter method for cnpj.
	 *
	 * @param aCnpj the new value for cnpj
	 */
	public void setCnpj(String aCnpj) {
		cnpj = aCnpj;
	}

	/**
	 * Access method for active.
	 *
	 * @return the current value of active
	 */
	public short getActive() {
		return active;
	}

	/**
	 * Setter method for active.
	 *
	 * @param aActive the new value for active
	 */
	public void setActive(short aActive) {
		active = aActive;
	}

	/**
	 * Access method for emailPartner.
	 *
	 * @return the current value of emailPartner
	 */
	public String getEmailPartner() {
		return emailPartner;
	}

	/**
	 * Setter method for emailPartner.
	 *
	 * @param aEmailPartner the new value for emailPartner
	 */
	public void setEmailPartner(String aEmailPartner) {
		emailPartner = aEmailPartner;
	}

	/**
	 * Access method for cellPhone.
	 *
	 * @return the current value of cellPhone
	 */
	public String getCellPhone() {
		return cellPhone;
	}

	/**
	 * Setter method for cellPhone.
	 *
	 * @param aCellPhone the new value for cellPhone
	 */
	public void setCellPhone(String aCellPhone) {
		cellPhone = aCellPhone;
	}

	/**
	 * Access method for phone.
	 *
	 * @return the current value of phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Setter method for phone.
	 *
	 * @param aPhone the new value for phone
	 */
	public void setPhone(String aPhone) {
		phone = aPhone;
	}

	/**
	 * Access method for dateCreated.
	 *
	 * @return the current value of dateCreated
	 */
	public Timestamp getDateCreated() {
		return dateCreated;
	}

	/**
	 * Setter method for dateCreated.
	 *
	 * @param aDateCreated the new value for dateCreated
	 */
	public void setDateCreated(Timestamp aDateCreated) {
		dateCreated = aDateCreated;
	}

	/**
	 * Access method for dateUpdated.
	 *
	 * @return the current value of dateUpdated
	 */
	public Timestamp getDateUpdated() {
		return dateUpdated;
	}

	/**
	 * Setter method for dateUpdated.
	 *
	 * @param aDateUpdated the new value for dateUpdated
	 */
	public void setDateUpdated(Timestamp aDateUpdated) {
		dateUpdated = aDateUpdated;
	}

	/**
	 * Access method for product.
	 *
	 * @return the current value of product
	 */
	public Set<Product> getProduct() {
		return product;
	}

	/**
	 * Setter method for product.
	 *
	 * @param aProduct the new value for product
	 */
	public void setProduct(Set<Product> aProduct) {
		product = aProduct;
	}

	/**
	 * Access method for pet.
	 *
	 * @return the current value of pet
	 */
	public Pet getPet() {
		return pet;
	}

	/**
	 * Setter method for pet.
	 *
	 * @param aPet the new value for pet
	 */
	public void setPet(Pet aPet) {
		pet = aPet;
	}

	/**
	 * Access method for stock.
	 *
	 * @return the current value of stock
	 */
	public Set<Stock> getStock() {
		return stock;
	}

	/**
	 * Setter method for stock.
	 *
	 * @param aStock the new value for stock
	 */
	public void setStock(Set<Stock> aStock) {
		stock = aStock;
	}

	/**
	 * Access method for store.
	 *
	 * @return the current value of store
	 */
	public Store getStore() {
		return store;
	}

	/**
	 * Setter method for store.
	 *
	 * @param aStore the new value for store
	 */
	public void setStore(Store aStore) {
		store = aStore;
	}

	/**
	 * Compares the key for this instance with another Partner.
	 *
	 * @param other The object to compare to
	 * @return True if other object is instance of class Partner and the key objects
	 *         are equal
	 */
	private boolean equalKeys(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Partner)) {
			return false;
		}
		Partner that = (Partner) other;
		if (this.getPartnerId() != that.getPartnerId()) {
			return false;
		}
		return true;
	}

	/**
	 * Compares this instance with another Partner.
	 *
	 * @param other The object to compare to
	 * @return True if the objects are the same
	 */
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Partner))
			return false;
		return this.equalKeys(other) && ((Partner) other).equalKeys(this);
	}

	/**
	 * Returns a hash code for this instance.
	 *
	 * @return Hash code
	 */
	@Override
	public int hashCode() {
		int i;
		int result = 17;
		i = getPartnerId();
		result = 37 * result + i;
		return result;
	}

	/**
	 * Returns a debug-friendly String representation of this instance.
	 *
	 * @return String representation of this instance
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("[Partner |");
		sb.append(" partnerId=").append(getPartnerId());
		sb.append("]");
		return sb.toString();
	}

	/**
	 * Return all elements of the primary key.
	 *
	 * @return Map of key names to values
	 */
	public Map<String, Object> getPrimaryKey() {
		Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
		ret.put("partnerId", Integer.valueOf(getPartnerId()));
		return ret;
	}

}
