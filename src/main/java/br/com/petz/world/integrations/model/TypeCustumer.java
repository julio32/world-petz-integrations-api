// Generated with g9.

package br.com.petz.world.integrations.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity(name="type_custumer")
public class TypeCustumer implements Serializable {

    /** Primary key. */
    protected static final String PK = "typeCustumerId";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @Id
    @Column(name="type_custumer_id", unique=true, nullable=false, length=10)
    private int typeCustumerId;
    @Column(name="name_type", nullable=false, length=50)
    private String nameType;
    @OneToOne(mappedBy="typeCustumer")
    private Customer customer;

    /** Default constructor. */
    public TypeCustumer() {
        super();
    }

    /**
     * Access method for typeCustumerId.
     *
     * @return the current value of typeCustumerId
     */
    public int getTypeCustumerId() {
        return typeCustumerId;
    }

    /**
     * Setter method for typeCustumerId.
     *
     * @param aTypeCustumerId the new value for typeCustumerId
     */
    public void setTypeCustumerId(int aTypeCustumerId) {
        typeCustumerId = aTypeCustumerId;
    }

    /**
     * Access method for nameType.
     *
     * @return the current value of nameType
     */
    public String getNameType() {
        return nameType;
    }

    /**
     * Setter method for nameType.
     *
     * @param aNameType the new value for nameType
     */
    public void setNameType(String aNameType) {
        nameType = aNameType;
    }

    /**
     * Access method for customer.
     *
     * @return the current value of customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Setter method for customer.
     *
     * @param aCustomer the new value for customer
     */
    public void setCustomer(Customer aCustomer) {
        customer = aCustomer;
    }

    /**
     * Compares the key for this instance with another TypeCustumer.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class TypeCustumer and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof TypeCustumer)) {
            return false;
        }
        TypeCustumer that = (TypeCustumer) other;
        if (this.getTypeCustumerId() != that.getTypeCustumerId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another TypeCustumer.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof TypeCustumer)) return false;
        return this.equalKeys(other) && ((TypeCustumer)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getTypeCustumerId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[TypeCustumer |");
        sb.append(" typeCustumerId=").append(getTypeCustumerId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("typeCustumerId", Integer.valueOf(getTypeCustumerId()));
        return ret;
    }

}
