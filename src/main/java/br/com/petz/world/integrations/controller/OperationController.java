package br.com.petz.world.integrations.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.petz.world.integrations.model.Customer;
import br.com.petz.world.integrations.services.OperationServices;

//Demonstração basica do código para validação forma e da estrutura 

@RestController
public class OperationController {

	@Autowired
	private OperationServices operationServices;

	@PostMapping("/petz/op/customer")
	public @ResponseBody Object salveCustomer(@RequestBody Customer customer) {

		return operationServices.saveCustumer(customer);

	}

	@PutMapping("/petz/op/customer")
	public @ResponseBody Object upadteCustomer(@RequestBody Customer customer) {

		return operationServices.saveCustumer(customer);

	}

	@GetMapping("/petz/op/customer")
	public @ResponseBody Object searchCustomerByDocument(@RequestBody Customer customer) {

		return operationServices.saveCustumer(customer);

	}

}
