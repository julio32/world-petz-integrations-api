package br.com.petz.world.integrations.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "type_product", indexes = { @Index(name = "type_product_name_IX", columnList = "name", unique = true) })
public class TypeProduct implements Serializable {

	/** Primary key. */
	protected static final String PK = "typeProductId";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "type_product_id", unique = true, nullable = false, length = 10)
	private int typeProductId;
	@Column(unique = true, nullable = false, length = 40)
	private String name;
	@OneToMany(mappedBy = "typeProduct")
	private Set<Product> product;

	/** Default constructor. */
	public TypeProduct() {
		super();
	}

	/**
	 * Access method for typeProductId.
	 *
	 * @return the current value of typeProductId
	 */
	public int getTypeProductId() {
		return typeProductId;
	}

	/**
	 * Setter method for typeProductId.
	 *
	 * @param aTypeProductId the new value for typeProductId
	 */
	public void setTypeProductId(int aTypeProductId) {
		typeProductId = aTypeProductId;
	}

	/**
	 * Access method for name.
	 *
	 * @return the current value of name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter method for name.
	 *
	 * @param aName the new value for name
	 */
	public void setName(String aName) {
		name = aName;
	}

	/**
	 * Access method for product.
	 *
	 * @return the current value of product
	 */
	public Set<Product> getProduct() {
		return product;
	}

	/**
	 * Setter method for product.
	 *
	 * @param aProduct the new value for product
	 */
	public void setProduct(Set<Product> aProduct) {
		product = aProduct;
	}

	/**
	 * Compares the key for this instance with another TypeProduct.
	 *
	 * @param other The object to compare to
	 * @return True if other object is instance of class TypeProduct and the key
	 *         objects are equal
	 */
	private boolean equalKeys(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TypeProduct)) {
			return false;
		}
		TypeProduct that = (TypeProduct) other;
		if (this.getTypeProductId() != that.getTypeProductId()) {
			return false;
		}
		return true;
	}

	/**
	 * Compares this instance with another TypeProduct.
	 *
	 * @param other The object to compare to
	 * @return True if the objects are the same
	 */
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof TypeProduct))
			return false;
		return this.equalKeys(other) && ((TypeProduct) other).equalKeys(this);
	}

	/**
	 * Returns a hash code for this instance.
	 *
	 * @return Hash code
	 */
	@Override
	public int hashCode() {
		int i;
		int result = 17;
		i = getTypeProductId();
		result = 37 * result + i;
		return result;
	}

	/**
	 * Returns a debug-friendly String representation of this instance.
	 *
	 * @return String representation of this instance
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("[TypeProduct |");
		sb.append(" typeProductId=").append(getTypeProductId());
		sb.append("]");
		return sb.toString();
	}

	/**
	 * Return all elements of the primary key.
	 *
	 * @return Map of key names to values
	 */
	public Map<String, Object> getPrimaryKey() {
		Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
		ret.put("typeProductId", Integer.valueOf(getTypeProductId()));
		return ret;
	}

}
