package br.com.petz.world.integrations.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.petz.world.integrations.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

}
