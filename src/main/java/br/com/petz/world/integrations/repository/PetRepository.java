package br.com.petz.world.integrations.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.petz.world.integrations.model.Pet;

@Repository
public interface PetRepository extends JpaRepository<Pet, Integer> {

}
