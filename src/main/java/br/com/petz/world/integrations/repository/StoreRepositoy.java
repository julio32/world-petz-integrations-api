package br.com.petz.world.integrations.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.petz.world.integrations.model.Store;

@Repository
public interface StoreRepositoy extends JpaRepository<Store, Integer> {

}
