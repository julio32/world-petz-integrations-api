package br.com.petz.world.integrations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@AutoConfigurationPackage
public class WorldPetzIntegrationsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorldPetzIntegrationsApiApplication.class, args);
	}

}
