package br.com.petz.world.integrations.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name = "customer")
public class Customer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "type_custumer_id", nullable = false, length = 10)
	private Integer typeCustumerId;
	@Column(nullable = false, length = 50)
	private String name;
	@Column(nullable = false, length = 50)
	private String document;
	@Column(length = 50)
	private String ie;
	@Column(nullable = false, length = 50)
	private String email;
	@Column(nullable = false, length = 2)
	private String state;
	@Column(nullable = false, length = 30)
	private String city;
	@Column(nullable = false, length = 30)
	private String cep;
	@Column(nullable = false, length = 30)
	private String street;
	@Column(nullable = false, length = 10)
	private int number;
	@Column(nullable = false, length = 30)
	private String complement;
	@Column(name = "cell_phone", nullable = false, length = 14)
	private String cellPhone;
	@Column(length = 13)
	private String phone;
	@Column(nullable = false, length = 3)
	private Boolean acitive;
	@OneToOne(optional = false, mappedBy = "customer")
	@JoinColumn(name = "customer_id", nullable = false)
	private TypeCustumer typeCustumer;

	/** Default constructor. */
	public Customer() {
		super();
	}

	/**
	 * Access method for typeCustumerId.
	 *
	 * @return the current value of typeCustumerId
	 */
	public int getTypeCustumerId() {
		return typeCustumerId;
	}

	/**
	 * Setter method for typeCustumerId.
	 *
	 * @param aTypeCustumerId the new value for typeCustumerId
	 */
	public void setTypeCustumerId(int aTypeCustumerId) {
		typeCustumerId = aTypeCustumerId;
	}

	/**
	 * Access method for name.
	 *
	 * @return the current value of name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter method for name.
	 *
	 * @param aName the new value for name
	 */
	public void setName(String aName) {
		name = aName;
	}

	/**
	 * Access method for document.
	 *
	 * @return the current value of document
	 */
	public String getDocument() {
		return document;
	}

	/**
	 * Setter method for document.
	 *
	 * @param aDocument the new value for document
	 */
	public void setDocument(String aDocument) {
		document = aDocument;
	}

	/**
	 * Access method for ie.
	 *
	 * @return the current value of ie
	 */
	public String getIe() {
		return ie;
	}

	/**
	 * Setter method for ie.
	 *
	 * @param aIe the new value for ie
	 */
	public void setIe(String aIe) {
		ie = aIe;
	}

	/**
	 * Access method for email.
	 *
	 * @return the current value of email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter method for email.
	 *
	 * @param aEmail the new value for email
	 */
	public void setEmail(String aEmail) {
		email = aEmail;
	}

	/**
	 * Access method for state.
	 *
	 * @return the current value of state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Setter method for state.
	 *
	 * @param aState the new value for state
	 */
	public void setState(String aState) {
		state = aState;
	}

	/**
	 * Access method for city.
	 *
	 * @return the current value of city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Setter method for city.
	 *
	 * @param aCity the new value for city
	 */
	public void setCity(String aCity) {
		city = aCity;
	}

	/**
	 * Access method for cep.
	 *
	 * @return the current value of cep
	 */
	public String getCep() {
		return cep;
	}

	/**
	 * Setter method for cep.
	 *
	 * @param aCep the new value for cep
	 */
	public void setCep(String aCep) {
		cep = aCep;
	}

	/**
	 * Access method for street.
	 *
	 * @return the current value of street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Setter method for street.
	 *
	 * @param aStreet the new value for street
	 */
	public void setStreet(String aStreet) {
		street = aStreet;
	}

	/**
	 * Access method for number.
	 *
	 * @return the current value of number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Setter method for number.
	 *
	 * @param aNumber the new value for number
	 */
	public void setNumber(int aNumber) {
		number = aNumber;
	}

	/**
	 * Access method for complement.
	 *
	 * @return the current value of complement
	 */
	public String getComplement() {
		return complement;
	}

	/**
	 * Setter method for complement.
	 *
	 * @param aComplement the new value for complement
	 */
	public void setComplement(String aComplement) {
		complement = aComplement;
	}

	/**
	 * Access method for cellPhone.
	 *
	 * @return the current value of cellPhone
	 */
	public String getCellPhone() {
		return cellPhone;
	}

	/**
	 * Setter method for cellPhone.
	 *
	 * @param aCellPhone the new value for cellPhone
	 */
	public void setCellPhone(String aCellPhone) {
		cellPhone = aCellPhone;
	}

	/**
	 * Access method for phone.
	 *
	 * @return the current value of phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Setter method for phone.
	 *
	 * @param aPhone the new value for phone
	 */
	public void setPhone(String aPhone) {
		phone = aPhone;
	}

	public Boolean getAcitive() {
		return acitive;
	}

	public void setAcitive(Boolean acitive) {
		this.acitive = acitive;
	}

	/**
	 * Access method for typeCustumer.
	 *
	 * @return the current value of typeCustumer
	 */
	public TypeCustumer getTypeCustumer() {
		return typeCustumer;
	}

	/**
	 * Setter method for typeCustumer.
	 *
	 * @param aTypeCustumer the new value for typeCustumer
	 */
	public void setTypeCustumer(TypeCustumer aTypeCustumer) {
		typeCustumer = aTypeCustumer;
	}

}
