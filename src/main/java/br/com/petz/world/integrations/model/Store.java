// Generated with g9.

package br.com.petz.world.integrations.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "store", indexes = { @Index(name = "store_cnpj_IX", columnList = "cnpj", unique = true),
		@Index(name = "store_email_IX", columnList = "email", unique = true),
		@Index(name = "store_manager_IX", columnList = "manager", unique = true) })
public class Store implements Serializable {

	/** Primary key. */
	protected static final String PK = "storeId";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "store_id", unique = true, nullable = false, length = 10)
	private int storeId;
	@Column(unique = true, nullable = false, length = 50)
	private String cnpj;
	@Column(unique = true, nullable = false, length = 50)
	private String email;
	@Column(unique = true, nullable = false, length = 50)
	private String manager;
	@Column(nullable = false, length = 2)
	private String state;
	@Column(nullable = false, length = 30)
	private String city;
	@Column(nullable = false, length = 30)
	private String cep;
	@Column(nullable = false, length = 30)
	private String street;
	@Column(nullable = false, length = 10)
	private int number;
	@Column(nullable = false, length = 30)
	private String complement;
	@Column(name = "cell_phone", nullable = false, length = 14)
	private String cellPhone;
	@Column(nullable = false, length = 13)
	private String phone;
	@Column(length = 1)
	private boolean active;
	@OneToMany(mappedBy = "store")
	private Set<Stock> stock;
	@OneToMany(mappedBy = "store")
	private Set<Partner> partner;

	/** Default constructor. */
	public Store() {
		super();
	}

	/**
	 * Access method for storeId.
	 *
	 * @return the current value of storeId
	 */
	public int getStoreId() {
		return storeId;
	}

	/**
	 * Setter method for storeId.
	 *
	 * @param aStoreId the new value for storeId
	 */
	public void setStoreId(int aStoreId) {
		storeId = aStoreId;
	}

	/**
	 * Access method for cnpj.
	 *
	 * @return the current value of cnpj
	 */
	public String getCnpj() {
		return cnpj;
	}

	/**
	 * Setter method for cnpj.
	 *
	 * @param aCnpj the new value for cnpj
	 */
	public void setCnpj(String aCnpj) {
		cnpj = aCnpj;
	}

	/**
	 * Access method for email.
	 *
	 * @return the current value of email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter method for email.
	 *
	 * @param aEmail the new value for email
	 */
	public void setEmail(String aEmail) {
		email = aEmail;
	}

	/**
	 * Access method for manager.
	 *
	 * @return the current value of manager
	 */
	public String getManager() {
		return manager;
	}

	/**
	 * Setter method for manager.
	 *
	 * @param aManager the new value for manager
	 */
	public void setManager(String aManager) {
		manager = aManager;
	}

	/**
	 * Access method for state.
	 *
	 * @return the current value of state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Setter method for state.
	 *
	 * @param aState the new value for state
	 */
	public void setState(String aState) {
		state = aState;
	}

	/**
	 * Access method for city.
	 *
	 * @return the current value of city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Setter method for city.
	 *
	 * @param aCity the new value for city
	 */
	public void setCity(String aCity) {
		city = aCity;
	}

	/**
	 * Access method for cep.
	 *
	 * @return the current value of cep
	 */
	public String getCep() {
		return cep;
	}

	/**
	 * Setter method for cep.
	 *
	 * @param aCep the new value for cep
	 */
	public void setCep(String aCep) {
		cep = aCep;
	}

	/**
	 * Access method for street.
	 *
	 * @return the current value of street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Setter method for street.
	 *
	 * @param aStreet the new value for street
	 */
	public void setStreet(String aStreet) {
		street = aStreet;
	}

	/**
	 * Access method for number.
	 *
	 * @return the current value of number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Setter method for number.
	 *
	 * @param aNumber the new value for number
	 */
	public void setNumber(int aNumber) {
		number = aNumber;
	}

	/**
	 * Access method for complement.
	 *
	 * @return the current value of complement
	 */
	public String getComplement() {
		return complement;
	}

	/**
	 * Setter method for complement.
	 *
	 * @param aComplement the new value for complement
	 */
	public void setComplement(String aComplement) {
		complement = aComplement;
	}

	/**
	 * Access method for cellPhone.
	 *
	 * @return the current value of cellPhone
	 */
	public String getCellPhone() {
		return cellPhone;
	}

	/**
	 * Setter method for cellPhone.
	 *
	 * @param aCellPhone the new value for cellPhone
	 */
	public void setCellPhone(String aCellPhone) {
		cellPhone = aCellPhone;
	}

	/**
	 * Access method for phone.
	 *
	 * @return the current value of phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Setter method for phone.
	 *
	 * @param aPhone the new value for phone
	 */
	public void setPhone(String aPhone) {
		phone = aPhone;
	}

	/**
	 * Access method for active.
	 *
	 * @return true if and only if active is currently true
	 */
	public boolean getActive() {
		return active;
	}

	/**
	 * Setter method for active.
	 *
	 * @param aActive the new value for active
	 */
	public void setActive(boolean aActive) {
		active = aActive;
	}

	/**
	 * Access method for stock.
	 *
	 * @return the current value of stock
	 */
	public Set<Stock> getStock() {
		return stock;
	}

	/**
	 * Setter method for stock.
	 *
	 * @param aStock the new value for stock
	 */
	public void setStock(Set<Stock> aStock) {
		stock = aStock;
	}

	/**
	 * Access method for partner.
	 *
	 * @return the current value of partner
	 */
	public Set<Partner> getPartner() {
		return partner;
	}

	/**
	 * Setter method for partner.
	 *
	 * @param aPartner the new value for partner
	 */
	public void setPartner(Set<Partner> aPartner) {
		partner = aPartner;
	}

	/**
	 * Compares the key for this instance with another Store.
	 *
	 * @param other The object to compare to
	 * @return True if other object is instance of class Store and the key objects
	 *         are equal
	 */
	private boolean equalKeys(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Store)) {
			return false;
		}
		Store that = (Store) other;
		if (this.getStoreId() != that.getStoreId()) {
			return false;
		}
		return true;
	}

	/**
	 * Compares this instance with another Store.
	 *
	 * @param other The object to compare to
	 * @return True if the objects are the same
	 */
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Store))
			return false;
		return this.equalKeys(other) && ((Store) other).equalKeys(this);
	}

	/**
	 * Returns a hash code for this instance.
	 *
	 * @return Hash code
	 */
	@Override
	public int hashCode() {
		int i;
		int result = 17;
		i = getStoreId();
		result = 37 * result + i;
		return result;
	}

	/**
	 * Returns a debug-friendly String representation of this instance.
	 *
	 * @return String representation of this instance
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("[Store |");
		sb.append(" storeId=").append(getStoreId());
		sb.append("]");
		return sb.toString();
	}

	/**
	 * Return all elements of the primary key.
	 *
	 * @return Map of key names to values
	 */
	public Map<String, Object> getPrimaryKey() {
		Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
		ret.put("storeId", Integer.valueOf(getStoreId()));
		return ret;
	}

}
