package br.com.petz.world.integrations.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.petz.world.integrations.model.Customer;
import br.com.petz.world.integrations.model.Partner;
import br.com.petz.world.integrations.model.Pet;
import br.com.petz.world.integrations.repository.CustomerRepository;
import br.com.petz.world.integrations.repository.PartnerRepository;
import br.com.petz.world.integrations.repository.PetRepository;

/**
 * Service utilizada para apoiar os contratos Endpoints
 * 
 * @author julio
 *
 */

@Service
public class OperationServices {
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private PetRepository petRepository;
	@Autowired
	private PartnerRepository partnerRepository;
	private Customer customer;
	private Pet pet;
	private Partner partner;

	// Operacao cliente

	/**
	 * Operação: salvar clinete
	 * 
	 * @param Customer
	 * @return Object serializado ResponseEntity<>
	 */
	public Object saveCustumer(@Valid Customer customer) {
		if ((this.customer = customerRepository.save(customer)) != customer) {

			return new ResponseEntity<>(customer, HttpStatus.CREATED);
		} else {

			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/**
	 * Operação: deletar o clinete especifico
	 * 
	 * @param document (Cpf ou Cnpj)
	 * @return Object serializado ResponseEntity<>
	 */
	public Object deleteCustumerCpfOrCnpj(String document) {

		if ((this.customer = customerRepository.findByDocument(document)) != null) {

			customerRepository.delete(this.customer);

			return new ResponseEntity<>(this.customer, HttpStatus.ACCEPTED);
		} else {

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

		}

	}

	/**
	 * Operação: busca o clinete especifico
	 * 
	 * @param document (Cpf ou Cnpj)
	 * @return Object serializado ResponseEntity<>
	 */
	public ResponseEntity<Object> searchCustomerByCpfOrCnpj(String document) {

		if ((this.customer = customerRepository.findByDocument(document)) != null) {

			return new ResponseEntity<>(this.customer, HttpStatus.OK);
		} else {

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

		}

	}

	/**
	 * Operação: busca todos os clinetes
	 * 
	 * @return Object serializado ResponseEntity<> ou List<Customer>
	 */

	public Object searchCustomer() {

		final List<Customer> lisCustomers = customerRepository.findAll();

		if (lisCustomers.isEmpty()) {

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return lisCustomers;

	}

	/**
	 * Operação: atualizar o cliente especifico
	 * 
	 * @param Customer
	 * @return Object serializado ResponseEntity<>
	 */
	public Object updateCustomer(@Valid Customer customerUpdate) {

		if ((this.customer = customerRepository.save(customerUpdate)) != customerUpdate) {

			return new ResponseEntity<>(customer, HttpStatus.OK);
		} else {

			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	// Operacao pet

	/**
	 * Operação: salvar Pet(Cadastrar tipo do pet)
	 * 
	 * @param Pet
	 * @return Object serializado ResponseEntity<>
	 */
	public Object savePet(@Valid Pet pet) {
		if ((this.pet = petRepository.save(pet)) != pet) {

			return new ResponseEntity<>(this.pet, HttpStatus.CREATED);
		} else {

			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/**
	 * Operação: deletar o Pet (Cadastro tipo pet)
	 * 
	 * @param idPet
	 * @return Object serializado ResponseEntity<>
	 */
	public Object deletePetById(Integer idPet) {

		if ((this.pet = petRepository.findById(idPet).get()) != null) {

			petRepository.delete(this.pet);

			return new ResponseEntity<>(this.pet, HttpStatus.ACCEPTED);
		} else {

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

		}

	}

	/**
	 * Operação: busca o Pet (Cadastro tipo pet)
	 * 
	 * @param idPet
	 * @return Object serializado ResponseEntity<>
	 */
	public ResponseEntity<Object> searchPetById(Integer idPet) {

		if ((this.pet = petRepository.findById(idPet).get()) != null) {

			return new ResponseEntity<>(this.pet, HttpStatus.OK);
		} else {

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

		}

	}

	/**
	 * Operação: busca todos os Pets type
	 * 
	 * @return Object serializado ResponseEntity<> ou List<Pet>
	 */

	public Object searchPet() {

		final List<Pet> lisPet = petRepository.findAll();

		if (lisPet.isEmpty()) {

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return lisPet;

	}

	/**
	 * Operação: atualizar o pet especifico
	 * 
	 * @param petUpdate
	 * @return Object serializado ResponseEntity<>
	 */
	public Object updatePet(@Valid Pet petUpdate) {

		if ((this.pet = petRepository.save(petUpdate)) != petUpdate) {

			return new ResponseEntity<>(petUpdate, HttpStatus.OK);
		} else {

			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	// Operação Parceiros/Fornecedores

	/**
	 * Operação: salvar parceiros
	 * 
	 * @param Partner
	 * @return Object serializado ResponseEntity<>
	 */
	public Object saveCustumer(@Valid Partner partner) {
		if ((this.partner = partnerRepository.save(partner)) != partner) {

			return new ResponseEntity<>(this.partner, HttpStatus.CREATED);
		} else {

			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	/**
	 * Operação: deletar o parceiro
	 * 
	 * @param cnpj
	 * @return Object serializado ResponseEntity<>
	 */
	public Object deletePartnerById(Integer partnerId) {

		if ((this.partner = partnerRepository.findById(partnerId).get()) != null) {

			partnerRepository.delete(this.partner);

			return new ResponseEntity<>(this.partner, HttpStatus.ACCEPTED);
		} else {

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

		}

	}

}
