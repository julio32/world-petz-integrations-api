// Generated with g9.

package br.com.petz.world.integrations.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "pet", indexes = { @Index(name = "pet_type_IX", columnList = "type", unique = true) })
public class Pet implements Serializable {

	/** Primary key. */
	protected static final String PK = "idPet";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pet", unique = true, nullable = false, length = 10)
	private int idPet;
	@Column(unique = true, nullable = false, length = 50)
	private String type;
	@Column(name = "date_created", nullable = false)
	private Timestamp dateCreated;
	@Column(name = "date_updated")
	private Timestamp dateUpdated;
	@OneToMany(mappedBy = "pet")
	private Set<Partner> partner;
	@OneToMany(mappedBy = "pet")
	private Set<Product> product;

	/** Default constructor. */
	public Pet() {
		super();
	}

	/**
	 * Access method for idPet.
	 *
	 * @return the current value of idPet
	 */
	public int getIdPet() {
		return idPet;
	}

	/**
	 * Setter method for idPet.
	 *
	 * @param aIdPet the new value for idPet
	 */
	public void setIdPet(int aIdPet) {
		idPet = aIdPet;
	}

	/**
	 * Access method for type.
	 *
	 * @return the current value of type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Setter method for type.
	 *
	 * @param aType the new value for type
	 */
	public void setType(String aType) {
		type = aType;
	}

	/**
	 * Access method for dateCreated.
	 *
	 * @return the current value of dateCreated
	 */
	public Timestamp getDateCreated() {
		return dateCreated;
	}

	/**
	 * Setter method for dateCreated.
	 *
	 * @param aDateCreated the new value for dateCreated
	 */
	public void setDateCreated(Timestamp aDateCreated) {
		dateCreated = aDateCreated;
	}

	/**
	 * Access method for dateUpdated.
	 *
	 * @return the current value of dateUpdated
	 */
	public Timestamp getDateUpdated() {
		return dateUpdated;
	}

	/**
	 * Setter method for dateUpdated.
	 *
	 * @param aDateUpdated the new value for dateUpdated
	 */
	public void setDateUpdated(Timestamp aDateUpdated) {
		dateUpdated = aDateUpdated;
	}

	/**
	 * Access method for partner.
	 *
	 * @return the current value of partner
	 */
	public Set<Partner> getPartner() {
		return partner;
	}

	/**
	 * Setter method for partner.
	 *
	 * @param aPartner the new value for partner
	 */
	public void setPartner(Set<Partner> aPartner) {
		partner = aPartner;
	}

	/**
	 * Access method for product.
	 *
	 * @return the current value of product
	 */
	public Set<Product> getProduct() {
		return product;
	}

	/**
	 * Setter method for product.
	 *
	 * @param aProduct the new value for product
	 */
	public void setProduct(Set<Product> aProduct) {
		product = aProduct;
	}

	/**
	 * Compares the key for this instance with another Pet.
	 *
	 * @param other The object to compare to
	 * @return True if other object is instance of class Pet and the key objects are
	 *         equal
	 */
	private boolean equalKeys(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Pet)) {
			return false;
		}
		Pet that = (Pet) other;
		if (this.getIdPet() != that.getIdPet()) {
			return false;
		}
		return true;
	}

	/**
	 * Compares this instance with another Pet.
	 *
	 * @param other The object to compare to
	 * @return True if the objects are the same
	 */
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Pet))
			return false;
		return this.equalKeys(other) && ((Pet) other).equalKeys(this);
	}

	/**
	 * Returns a hash code for this instance.
	 *
	 * @return Hash code
	 */
	@Override
	public int hashCode() {
		int i;
		int result = 17;
		i = getIdPet();
		result = 37 * result + i;
		return result;
	}

	/**
	 * Returns a debug-friendly String representation of this instance.
	 *
	 * @return String representation of this instance
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("[Pet |");
		sb.append(" idPet=").append(getIdPet());
		sb.append("]");
		return sb.toString();
	}

	/**
	 * Return all elements of the primary key.
	 *
	 * @return Map of key names to values
	 */
	public Map<String, Object> getPrimaryKey() {
		Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
		ret.put("idPet", Integer.valueOf(getIdPet()));
		return ret;
	}

}
