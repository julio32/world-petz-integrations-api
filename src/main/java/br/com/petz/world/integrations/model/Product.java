// Generated with g9.

package br.com.petz.world.integrations.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity(name = "product")
public class Product implements Serializable {

	/** Primary key. */
	protected static final String PK = "productId";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id", unique = true, nullable = false, length = 10)
	private int productId;
	@Column(name = "name_product", nullable = false, length = 50)
	private String nameProduct;
	@Column(length = 50)
	private String description;
	@Column(name = "price_unit", nullable = false, precision = 10, scale = 3)
	private BigDecimal priceUnit;
	@Column(name = "price_cost", nullable = false, precision = 10, scale = 3)
	private BigDecimal priceCost;
	@Column(name = "date_created", nullable = false)
	private Timestamp dateCreated;
	@Column(name = "date_updated")
	private Timestamp dateUpdated;
	@ManyToOne(optional = false)
	@JoinColumn(name = "partner_id", nullable = false)
	private Partner partner;
	@ManyToOne(optional = false)
	@JoinColumn(name = "id_pet", nullable = false)
	private Pet pet;
	@OneToOne(optional = false, mappedBy = "product2")
	@JoinColumn(name = "stock_id", nullable = false)
	private Stock stock2;
	@OneToMany(mappedBy = "product")
	private Set<Stock> stock;
	@ManyToOne(optional = false)
	@JoinColumn(name = "type_product_id", nullable = false)
	private TypeProduct typeProduct;

	/** Default constructor. */
	public Product() {
		super();
	}

	/**
	 * Access method for productId.
	 *
	 * @return the current value of productId
	 */
	public int getProductId() {
		return productId;
	}

	/**
	 * Setter method for productId.
	 *
	 * @param aProductId the new value for productId
	 */
	public void setProductId(int aProductId) {
		productId = aProductId;
	}

	/**
	 * Access method for nameProduct.
	 *
	 * @return the current value of nameProduct
	 */
	public String getNameProduct() {
		return nameProduct;
	}

	/**
	 * Setter method for nameProduct.
	 *
	 * @param aNameProduct the new value for nameProduct
	 */
	public void setNameProduct(String aNameProduct) {
		nameProduct = aNameProduct;
	}

	/**
	 * Access method for description.
	 *
	 * @return the current value of description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setter method for description.
	 *
	 * @param aDescription the new value for description
	 */
	public void setDescription(String aDescription) {
		description = aDescription;
	}

	/**
	 * Access method for priceUnit.
	 *
	 * @return the current value of priceUnit
	 */
	public BigDecimal getPriceUnit() {
		return priceUnit;
	}

	/**
	 * Setter method for priceUnit.
	 *
	 * @param aPriceUnit the new value for priceUnit
	 */
	public void setPriceUnit(BigDecimal aPriceUnit) {
		priceUnit = aPriceUnit;
	}

	/**
	 * Access method for priceCost.
	 *
	 * @return the current value of priceCost
	 */
	public BigDecimal getPriceCost() {
		return priceCost;
	}

	/**
	 * Setter method for priceCost.
	 *
	 * @param aPriceCost the new value for priceCost
	 */
	public void setPriceCost(BigDecimal aPriceCost) {
		priceCost = aPriceCost;
	}

	/**
	 * Access method for dateCreated.
	 *
	 * @return the current value of dateCreated
	 */
	public Timestamp getDateCreated() {
		return dateCreated;
	}

	/**
	 * Setter method for dateCreated.
	 *
	 * @param aDateCreated the new value for dateCreated
	 */
	public void setDateCreated(Timestamp aDateCreated) {
		dateCreated = aDateCreated;
	}

	/**
	 * Access method for dateUpdated.
	 *
	 * @return the current value of dateUpdated
	 */
	public Timestamp getDateUpdated() {
		return dateUpdated;
	}

	/**
	 * Setter method for dateUpdated.
	 *
	 * @param aDateUpdated the new value for dateUpdated
	 */
	public void setDateUpdated(Timestamp aDateUpdated) {
		dateUpdated = aDateUpdated;
	}

	/**
	 * Access method for partner.
	 *
	 * @return the current value of partner
	 */
	public Partner getPartner() {
		return partner;
	}

	/**
	 * Setter method for partner.
	 *
	 * @param aPartner the new value for partner
	 */
	public void setPartner(Partner aPartner) {
		partner = aPartner;
	}

	/**
	 * Access method for pet.
	 *
	 * @return the current value of pet
	 */
	public Pet getPet() {
		return pet;
	}

	/**
	 * Setter method for pet.
	 *
	 * @param aPet the new value for pet
	 */
	public void setPet(Pet aPet) {
		pet = aPet;
	}

	/**
	 * Access method for stock2.
	 *
	 * @return the current value of stock2
	 */
	public Stock getStock2() {
		return stock2;
	}

	/**
	 * Setter method for stock2.
	 *
	 * @param aStock2 the new value for stock2
	 */
	public void setStock2(Stock aStock2) {
		stock2 = aStock2;
	}

	/**
	 * Access method for stock.
	 *
	 * @return the current value of stock
	 */
	public Set<Stock> getStock() {
		return stock;
	}

	/**
	 * Setter method for stock.
	 *
	 * @param aStock the new value for stock
	 */
	public void setStock(Set<Stock> aStock) {
		stock = aStock;
	}

	/**
	 * Access method for typeProduct.
	 *
	 * @return the current value of typeProduct
	 */
	public TypeProduct getTypeProduct() {
		return typeProduct;
	}

	/**
	 * Setter method for typeProduct.
	 *
	 * @param aTypeProduct the new value for typeProduct
	 */
	public void setTypeProduct(TypeProduct aTypeProduct) {
		typeProduct = aTypeProduct;
	}

	/**
	 * Compares the key for this instance with another Product.
	 *
	 * @param other The object to compare to
	 * @return True if other object is instance of class Product and the key objects
	 *         are equal
	 */
	private boolean equalKeys(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Product)) {
			return false;
		}
		Product that = (Product) other;
		if (this.getProductId() != that.getProductId()) {
			return false;
		}
		return true;
	}

	/**
	 * Compares this instance with another Product.
	 *
	 * @param other The object to compare to
	 * @return True if the objects are the same
	 */
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Product))
			return false;
		return this.equalKeys(other) && ((Product) other).equalKeys(this);
	}

	/**
	 * Returns a hash code for this instance.
	 *
	 * @return Hash code
	 */
	@Override
	public int hashCode() {
		int i;
		int result = 17;
		i = getProductId();
		result = 37 * result + i;
		return result;
	}

	/**
	 * Returns a debug-friendly String representation of this instance.
	 *
	 * @return String representation of this instance
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("[Product |");
		sb.append(" productId=").append(getProductId());
		sb.append("]");
		return sb.toString();
	}

	/**
	 * Return all elements of the primary key.
	 *
	 * @return Map of key names to values
	 */
	public Map<String, Object> getPrimaryKey() {
		Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
		ret.put("productId", Integer.valueOf(getProductId()));
		return ret;
	}

}
