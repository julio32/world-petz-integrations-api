// Generated with g9.

package br.com.petz.world.integrations.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity(name = "stock")
public class Stock implements Serializable {

	/** Primary key. */
	protected static final String PK = "stockId";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "stock_id", unique = true, nullable = false, length = 10)
	private int stockId;
	@Column(name = "max_stock", nullable = false, precision = 10, scale = 4)
	private BigDecimal maxStock;
	@Column(name = "min_stock", nullable = false, precision = 10, scale = 4)
	private BigDecimal minStock;
	@Column(name = "current_stock", precision = 10, scale = 4)
	private BigDecimal currentStock;
	@Column(name = "date_created", nullable = false)
	private Timestamp dateCreated;
	@Column(name = "date_updated")
	private Timestamp dateUpdated;
	@OneToOne(mappedBy = "stock2")
	private Product product2;
	@ManyToOne(optional = false)
	@JoinColumn(name = "partner_id", nullable = false)
	private Partner partner;
	@ManyToOne(optional = false)
	@JoinColumn(name = "product_id", nullable = false)
	private Product product;
	@ManyToOne
	@JoinColumn(name = "store_id")
	private Store store;

	/** Default constructor. */
	public Stock() {
		super();
	}

	/**
	 * Access method for stockId.
	 *
	 * @return the current value of stockId
	 */
	public int getStockId() {
		return stockId;
	}

	/**
	 * Setter method for stockId.
	 *
	 * @param aStockId the new value for stockId
	 */
	public void setStockId(int aStockId) {
		stockId = aStockId;
	}

	/**
	 * Access method for maxStock.
	 *
	 * @return the current value of maxStock
	 */
	public BigDecimal getMaxStock() {
		return maxStock;
	}

	/**
	 * Setter method for maxStock.
	 *
	 * @param aMaxStock the new value for maxStock
	 */
	public void setMaxStock(BigDecimal aMaxStock) {
		maxStock = aMaxStock;
	}

	/**
	 * Access method for minStock.
	 *
	 * @return the current value of minStock
	 */
	public BigDecimal getMinStock() {
		return minStock;
	}

	/**
	 * Setter method for minStock.
	 *
	 * @param aMinStock the new value for minStock
	 */
	public void setMinStock(BigDecimal aMinStock) {
		minStock = aMinStock;
	}

	/**
	 * Access method for currentStock.
	 *
	 * @return the current value of currentStock
	 */
	public BigDecimal getCurrentStock() {
		return currentStock;
	}

	/**
	 * Setter method for currentStock.
	 *
	 * @param aCurrentStock the new value for currentStock
	 */
	public void setCurrentStock(BigDecimal aCurrentStock) {
		currentStock = aCurrentStock;
	}

	/**
	 * Access method for dateCreated.
	 *
	 * @return the current value of dateCreated
	 */
	public Timestamp getDateCreated() {
		return dateCreated;
	}

	/**
	 * Setter method for dateCreated.
	 *
	 * @param aDateCreated the new value for dateCreated
	 */
	public void setDateCreated(Timestamp aDateCreated) {
		dateCreated = aDateCreated;
	}

	/**
	 * Access method for dateUpdated.
	 *
	 * @return the current value of dateUpdated
	 */
	public Timestamp getDateUpdated() {
		return dateUpdated;
	}

	/**
	 * Setter method for dateUpdated.
	 *
	 * @param aDateUpdated the new value for dateUpdated
	 */
	public void setDateUpdated(Timestamp aDateUpdated) {
		dateUpdated = aDateUpdated;
	}

	/**
	 * Access method for product2.
	 *
	 * @return the current value of product2
	 */
	public Product getProduct2() {
		return product2;
	}

	/**
	 * Setter method for product2.
	 *
	 * @param aProduct2 the new value for product2
	 */
	public void setProduct2(Product aProduct2) {
		product2 = aProduct2;
	}

	/**
	 * Access method for partner.
	 *
	 * @return the current value of partner
	 */
	public Partner getPartner() {
		return partner;
	}

	/**
	 * Setter method for partner.
	 *
	 * @param aPartner the new value for partner
	 */
	public void setPartner(Partner aPartner) {
		partner = aPartner;
	}

	/**
	 * Access method for product.
	 *
	 * @return the current value of product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * Setter method for product.
	 *
	 * @param aProduct the new value for product
	 */
	public void setProduct(Product aProduct) {
		product = aProduct;
	}

	/**
	 * Access method for store.
	 *
	 * @return the current value of store
	 */
	public Store getStore() {
		return store;
	}

	/**
	 * Setter method for store.
	 *
	 * @param aStore the new value for store
	 */
	public void setStore(Store aStore) {
		store = aStore;
	}

	/**
	 * Compares the key for this instance with another Stock.
	 *
	 * @param other The object to compare to
	 * @return True if other object is instance of class Stock and the key objects
	 *         are equal
	 */
	private boolean equalKeys(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Stock)) {
			return false;
		}
		Stock that = (Stock) other;
		if (this.getStockId() != that.getStockId()) {
			return false;
		}
		return true;
	}

	/**
	 * Compares this instance with another Stock.
	 *
	 * @param other The object to compare to
	 * @return True if the objects are the same
	 */
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Stock))
			return false;
		return this.equalKeys(other) && ((Stock) other).equalKeys(this);
	}

	/**
	 * Returns a hash code for this instance.
	 *
	 * @return Hash code
	 */
	@Override
	public int hashCode() {
		int i;
		int result = 17;
		i = getStockId();
		result = 37 * result + i;
		return result;
	}

	/**
	 * Returns a debug-friendly String representation of this instance.
	 *
	 * @return String representation of this instance
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("[Stock |");
		sb.append(" stockId=").append(getStockId());
		sb.append("]");
		return sb.toString();
	}

	/**
	 * Return all elements of the primary key.
	 *
	 * @return Map of key names to values
	 */
	public Map<String, Object> getPrimaryKey() {
		Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
		ret.put("stockId", Integer.valueOf(getStockId()));
		return ret;
	}

}
