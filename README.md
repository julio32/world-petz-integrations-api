# petz

 Tem como princial objetivo demonstrar forma basica como organizo estrutura do codigo e sua tecnologias 

História:
Eu cliente preciso de APIs REST para que possa integrar meus sistemas com outros parceiros.
Inicialmente, precisamos de APIs de integrações para:
• Cadastro
• Edição
• Exclusão
• Visualização
Das entidades do meu sistema:
• Cliente
• Pet
Requisitos técnicos:
- A persistência com o banco de dados deve ser utilizado o Hibernate;
- A arquitetura deve ser implementado em MPV ou MVVM;
- O framework utilizado deve ser o Spring-boot;
Requisitos de entrega:
- O código deve ser armazenado em um repositório GIT (como github)
- As chaves de acesso não devem ser armazenadas no repositório (.gitignore)